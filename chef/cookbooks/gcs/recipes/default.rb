template '/var/lib/pgsql/data/pg_hba.conf' do
	source "pg_hba.conf.erb"
	variables ({
		:pg_subred => node['gcs']['pg_subred'],
		:pg_mode => node['gcs']['pg_mode']		
	})
end

template '/var/lib/pgsql/data/postgresql.conf' do
	source "postgresql.conf.erb"
	variables ({
		:pg_listen_addresses => node['gcs']['pg_listen_addresses']
	})
end